package clases2;

import java.time.LocalDate;

public class ListaEmpresa {
private Empresa[] empresas;
/*
 * constructor para el maximo de empresas que va ha tener el programa
 */
	public ListaEmpresa(int maxEmpresas) {
		this.empresas=new Empresa[maxEmpresas];
	}
	/*
	 * constructor con todos los atributos de la clase empresa
	 */
	public void altaEmpresa(String nombre,String asociados,int capital,String fechaCreacion) {
		for(int i=0;i<empresas.length;i++) {
			if(empresas[i]==null) {
				empresas[i]=new Empresa(nombre);
				empresas[i].setAsociados(asociados);
				empresas[i].setCapital(capital);
				empresas[i].setFechaCreacion(LocalDate.parse(fechaCreacion));
				break;
			}
		}
	}
	/*
	 * metodo para buscar las empresas que se le indique 
	 */
	public Empresa buscarEmpresas(String nombre) {
		for(int i=0;i<empresas.length;i++) {
			if(empresas[i]!=null) {
			if(empresas[i].getNombre().equals(nombre)) {
				return empresas[i];
			}
		  }
		}
		return null;
	}
	/*
	 * metodo que elimine las empresas que se le indique
	 */
	public void eliminarEmpresas(String nombre){
		for(int i=0;i<empresas.length;i++) {
			if(empresas[i]!=null) {
			if(empresas[i].getNombre().equals(nombre)) {
			empresas[i]=null;
			}
			}
		}
	}
	/*
	 * metodo que devuelva todas las empresas
	 */
	public void listarEmpresas() {
		for(int i=0;i<empresas.length;i++) {
			if(empresas[i]!=null) {
				System.out.println(empresas[i]);
			}
		}
	}
	/*
	 * metodo que cambie el asociado de la empresa indicada
	 */
	public void cambiarEmpresas(String nombre,String asocia2) {
		for(int i=0;i<empresas.length;i++) {
			if(empresas[i]!=null) {
			if(empresas[i].getNombre().equals(nombre)) {
				empresas[i].setAsociados(asocia2);
			}
		  }
		}
	}
	/*
	 * metodo que devuelva una lista de empresas por el asociado que le indique
	 */
	public void listarEmpresasPorAsociado(String asociados) {
		for(int i=0;i<empresas.length;i++) {
			if(empresas[i]!=null) {
			if(empresas[i].getAsociados().equals(asociados)) {
				System.out.println(empresas[i]);
			}
		  }
		}
	}
	/*
	 * metodo que elimine las empresas por el asociado que le indique
	 */
	public void eliminarEmpresasPorAsociado(String asociados) {
		for(int i=0;i<empresas.length;i++) {
			if(empresas[i]!=null) {
			if(empresas[i].getAsociados().equals(asociados)) {
				empresas[i]=null;
			}
		  }
		}
	}
}
