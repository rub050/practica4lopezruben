package clases2;

import java.time.LocalDate;

public class Empresa {
	private String nombre;
	private int capital;
	private String asociados;
	private LocalDate fechaCreacion;
	public Empresa(String nombre) {
		this.nombre = nombre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getCapital() {
		return capital;
	}
	public void setCapital(int capital) {
		this.capital = capital;
	}
	public String getAsociados() {
		return asociados;
	}
	public void setAsociados(String asociados) {
		this.asociados = asociados;
	}
	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(LocalDate FechaCreacion) {
		fechaCreacion = FechaCreacion;
	}
	@Override
	public String toString() {
		return "La empresa " + nombre + " con un capital total de " + capital + " su mejor contribuyente es " + asociados + " y fue creada el "
				+ fechaCreacion;
	}
	
	
	
	
	
}
