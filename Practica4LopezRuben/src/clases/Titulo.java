package clases;

import java.time.LocalDate;

public class Titulo {
private String nombre;
private String empresa;
private int usuarios;
private String tipo;
private String modoHistoria;
private double precio;
private LocalDate fechaLanzamiento;


public Titulo(String nombre) {
	this.nombre = nombre;
}

public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getEmpresa() {
	return empresa;
}
public void setEmpresa(String empresa) {
	this.empresa = empresa;
}
public int getUsuarios() {
	return usuarios;
}
public void setUsuarios(int usuarios) {
	this.usuarios = usuarios;
}
public String getTipo() {
	return tipo;
}
public void setTipo(String tipo) {
	this.tipo = tipo;
}
public String isModoHistoria() {
	return modoHistoria;
}
public void setModoHistoria(String modoHistoria) {
	this.modoHistoria = modoHistoria;
}

public double getPrecio() {
	return precio;
}

public void setPrecio(double precio) {
	this.precio = precio;
}

public LocalDate getFechaLanzamiento() {
	return fechaLanzamiento;
}

public void setFechaLanzamiento(LocalDate FechaLanzamiento) {
	fechaLanzamiento = FechaLanzamiento;
}
@Override
public String toString() {
	return "El videojuego " + nombre + " fue creado por la empresa " + empresa+ " en la fecha " + fechaLanzamiento + ", "+modoHistoria + " tiene modo historia" + ", es un juego " + tipo + " con " + usuarios 
			+" usuarios y con un precio de "+ precio;
}








}
