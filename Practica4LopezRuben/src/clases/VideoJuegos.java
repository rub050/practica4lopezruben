package clases;

import java.time.LocalDate;

public class VideoJuegos {
private Titulo[] titulos;
	/*
	 * constructor para el maximo de titulos que va ha tener el programa
	 */
	public VideoJuegos(int maxTitulos) {
		this.titulos=new Titulo[maxTitulos];
	}
	/*
	 * constructor con todos los atributos de la clase titulos
	 */
	
	public void altaTitulos(String nombre,String empresa,int usuario,String tipo,String modoHistoria,double precio,String FechaLanzamiento) {
		for(int i=0;i<titulos.length;i++) {
			if(titulos[i]==null) {
				titulos[i]=new Titulo(nombre);
				titulos[i].setEmpresa(empresa);
				titulos[i].setTipo(tipo);
				titulos[i].setModoHistoria(modoHistoria);
				titulos[i].setPrecio(precio);
				titulos[i].setFechaLanzamiento(LocalDate.parse(FechaLanzamiento));
				break;
			}
		}
	}
	
	/*
	 * metodo para buscar el titulo que se le indique 
	 */
	public Titulo buscarTitulo(String nombre) {
		for(int i=0;i<titulos.length;i++) {
			if(titulos[i]!=null) {
			if(titulos[i].getNombre().equals(nombre)) {
				return titulos[i];
			}
		  }
		}
		return null;
	}
	
	/*
	 * metodo que elimine los titulos que se le indique
	 */
	public void eliminarTitulo(String nombre){
		for(int i=0;i<titulos.length;i++) {
			if(titulos[i]!=null) {
			if(titulos[i].getNombre().equals(nombre)) {
			titulos[i]=null;
			}
			}
		}
	}
	/*
	 * metodo que devuelva todos los titulos 
	 */
	public void listarTitulos() {
		for(int i=0;i<titulos.length;i++) {
			if(titulos[i]!=null) {
				System.out.println(titulos[i]);
			}
		}
	}
	/*
	 * metodo que cambie la empresa del titulo indicado
	 */
	public void cambiarTitulos(String nombre,String empresa2) {
		for(int i=0;i<titulos.length;i++) {
			if(titulos[i]!=null) {
			if(titulos[i].getNombre().equals(nombre)) {
				titulos[i].setEmpresa(empresa2);
			}
		  }
		}
	}
	/*
	 * metodo que devuelva una lista de titulos por la empresa que le indique
	 */
	public void listarTitulosPorEmpresa(String empresa) {
		for(int i=0;i<titulos.length;i++) {
			if(titulos[i]!=null) {
			if(titulos[i].getEmpresa().equals(empresa)) {
				System.out.println(titulos[i]);
			}
		  }
		}
	}
	/*
	 * metodo que elimine los titulos por la empresa que le indique
	 */
	public void eliminarTitulosPorEmpresa(String empresa) {
		for(int i=0;i<titulos.length;i++) {
			if(titulos[i]!=null) {
			if(titulos[i].getEmpresa().equals(empresa)) {
				titulos[i]=null;
			}
		  }
		}
	}
	
}
