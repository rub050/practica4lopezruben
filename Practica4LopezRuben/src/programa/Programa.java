package programa;

import java.util.Scanner;

import clases.VideoJuegos;
import clases2.ListaEmpresa;
import clases3.ListaUsuarios;

public class Programa {

	public static void main(String[] args) {
		Scanner num=new Scanner(System.in);
		int opcion=0;
		@SuppressWarnings("unused")
		int max=4;
		/*
		 * arrays de cada clase
		 */
		VideoJuegos juegos =new VideoJuegos(4);
		ListaEmpresa empresas =new ListaEmpresa(4);
		ListaUsuarios usuarios =new ListaUsuarios(4);
		/*
		*crear instancia titulos
		*/
		juegos.altaTitulos("Rainbow Six Siege","Ubisoft",2000,"Shooter","no",8.99,("2014-05-04"));
		juegos.altaTitulos("Call of Duty","Activision",3500,"Shooter","si",39.99,("2006-08-05"));
		juegos.altaTitulos("Hearstone","Blizzard",1300,"Estrategia","si",0.00,("2010-02-09"));
		juegos.altaTitulos("Spider-Man","Treyarch",800,"Poderes","si",59.99,("2021-09-09"));
		/*
		*crear instancia empresas
		*/
		empresas.altaEmpresa("Activision","Larry",143,("1979-03-01"));
		empresas.altaEmpresa("Blizzard","Bobby",352,("1991-05-04"));
		empresas.altaEmpresa("Treyarch","Peter",734,("1996-02-09"));
		empresas.altaEmpresa("Ubisoft","Michel",126,("1986-01-05"));
		/*
		*crear instancia cuentas
		*/
		usuarios.altaCuenta("Evilman2002","Albaro",19,43.9,("2007-04-03"));
		usuarios.altaCuenta("Alex19PC","Alejandro",18,0.00,("2005-09-09"));
		usuarios.altaCuenta("Baquero555","Albaro",20,1.1,("2009-01-12"));
		usuarios.altaCuenta("Brother","Juan",28,12.4,("2008-03-07"));
		/*
		 * menu del programa 
		 */
			do {
			System.out.println("1-Listar");
			System.out.println("2-Buscar");
			System.out.println("3-Eliminar");
			System.out.println("4-Dar de alta");
			System.out.println("5-Modificar");
			System.out.println("6-Listar por atributo");
			System.out.println("7-Salir");
			System.out.println("Introduzca el numero de opcion:");
			opcion=num.nextInt();
			
				switch (opcion) {
				case 1:
						do {
						System.out.println("1-Listar Titulos");
						System.out.println("2-Listar empresas");
						System.out.println("3-Listar Cuentas");
						System.out.println("4-Salir");
						System.out.println("Introduzca el numero de opcion:");
						opcion=num.nextInt();
						
							switch (opcion) {
							case 1:
								/*
								 * llama al metodo listar titulos
								 */
								System.out.println("Lista de los videojuegos");
								juegos.listarTitulos();
								break;
							case 2:
								/*
								 * llama al metodo listar empresas
								 */
								System.out.println("Lista de las empresas");
								empresas.listarEmpresas();
								break;
							case 3:
								/*
								 * llama al metodo listar cuentas
								 */
								System.out.println("Lista de cuentas");
								usuarios.listarCuentas();
								break;
							case 4:
								break;
								default:
							System.out.println("opcion incorrecta");
							break;
							}
						}while(opcion!=4);
						
					

					break;
				case 2:
					do {
						System.out.println("1-Buscar Titulos");
						System.out.println("2-Buscar empresas");
						System.out.println("3-Buscar Cuentas");
						System.out.println("4-Salir");
						System.out.println("Introduzca el numero de opcion:");
						opcion=num.nextInt();
						
							switch (opcion) {
							case 1:
								/*
								 * llama al metodo buscar videojuegos
								 */
								num.nextLine();
								System.out.println("Introduce el videojuego que quieres buscar");
								String buscarVideojuego=num.nextLine();
								System.out.println(juegos.buscarTitulo(buscarVideojuego));
								break;
							case 2:
								/*
								 * llama al metodo buscar empresa
								 */
								num.nextLine();
								System.out.println("Introduce la empresa que quieres buscar");
								String buscarEmpresa=num.nextLine();
								System.out.println(empresas.buscarEmpresas(buscarEmpresa));
								break;
							case 3:
								/*
								 * llama al metodo buscar cuenta
								 */ 
								num.nextLine();
								System.out.println("Introduce la cuenta que quieres buscar");
								String buscarCuenta=num.nextLine();
								System.out.println(usuarios.buscarCuenta(buscarCuenta));
								break;
							case 4:
								break;
								default:
							System.out.println("opcion incorrecta");
							break;
							}
						}while(opcion!=4);
					break;
				case 3:
					do {
						System.out.println("1-Eliminar Titulos");
						System.out.println("2-Eliminar empresas");
						System.out.println("3-Eliminar Cuentas");
						System.out.println("4-Salir");
						System.out.println("Introduzca el numero de opcion:");
						opcion=num.nextInt();
						
							switch (opcion) {
							case 1:
								do {
									System.out.println("1-Eliminar titulos por su nombre");
									System.out.println("2-Eliminar titulos por nombre de la empresa");
									System.out.println("3-Salir");
									System.out.println("Introduzca el numero de opcion:");
									opcion=num.nextInt();
										switch (opcion) {
										case 1:
											/*
											 * llama al metodo eliminar titulos
											 */
											num.nextLine();
											System.out.println("Introduce el videojuego que quieres eliminar");
											String eliminarVideojuego=num.nextLine();
											juegos.eliminarTitulo(eliminarVideojuego);
											juegos.listarTitulos();
											break;
										case 2:
											/*
											 * llama al metodo eliminar titulo por empresa
											 */
											num.nextLine();
											System.out.println("Introduce la empresa que haya echo los videojuegos que quieres eliminar");
											String eliminarVideojuegoPorEmpresa=num.nextLine();
											juegos.eliminarTitulosPorEmpresa(eliminarVideojuegoPorEmpresa);
											juegos.listarTitulos();
											break;
										case 3:
											break;
											default:
												System.out.println("opcion incorrecta");
												break;
										}
									}while(opcion!=3);
								
								break;
							case 2:
								do {
									System.out.println("1-Eliminar empresa por su nombre");
									System.out.println("2-Eliminar empresa por nombre de el asociado");
									System.out.println("3-Salir");
									System.out.println("Introduzca el numero de opcion:");
									opcion=num.nextInt();
									
										switch (opcion) {
										case 1:
											/*
											 * llama al metodo eliminar empresas
											 */
											num.nextLine();
											System.out.println("Introduce la empresa que quieres eliminar");
											String eliminarEmpresa=num.nextLine();
											empresas.eliminarEmpresas(eliminarEmpresa);
											empresas.listarEmpresas();
											break;
										case 2:
											/*
											 * llama al metodo eliminar empresa por asociado
											 */
											num.nextLine();
											System.out.println("Introduce el nombre del asociado a las empresas que quieres eliminar");
											String eliminarEmpresaPorAsociado=num.nextLine();
											empresas.eliminarEmpresasPorAsociado(eliminarEmpresaPorAsociado);
											empresas.listarEmpresas();
											break;
										case 3:
											break;
											default:
												System.out.println("opcion incorrecta");
												break;
										}
									}while(opcion!=3);
								
								break;
							case 3:
								do {
									System.out.println("1-Eliminar cuenta por su nombre");
									System.out.println("2-Eliminar cuenta por nombre de el propietario");
									System.out.println("3-Salir");
									System.out.println("Introduzca el numero de opcion:");
									opcion=num.nextInt();
									
										switch (opcion) {
										case 1:
											/*
											 * llama al metodo eliminar cuenta
											 */
											num.nextLine();
											System.out.println("Introduce la cuenta que quieres eliminar");
											String eliminarCuenta=num.nextLine();
											usuarios.eliminarCuenta(eliminarCuenta);
											usuarios.listarCuentas();
											break;
										case 2:
											/*
											 * llama al metodo eliminar cuenta por nombre
											 */
											num.nextLine();
											System.out.println("Introduce la cuenta que quieres eliminar");
											String eliminarCuentaPorNombre=num.nextLine();
											usuarios.eliminarCuentaPorNombre(eliminarCuentaPorNombre);
											usuarios.listarCuentas();
											break;
										case 3:
											break;
											default:
												System.out.println("opcion incorrecta");
												break;
										}
									}while(opcion!=3);
								
								break;
							case 4:
								break;
								default:
							System.out.println("opcion incorrecta");
							break;
							}
						}while(opcion!=4);
					break;
				case 4:
					do {
						System.out.println("1-Dar de alta Titulos");
						System.out.println("2-Dar de alta empresas");
						System.out.println("3-Dar de alta Cuentas");
						System.out.println("4-Salir");
						System.out.println("Introduzca el numero de opcion:");
						opcion=num.nextInt();
						
							switch (opcion) {
							case 1:
								/*
								 * da de alta un nuevo titulo
								 */
								System.out.println("Previamente has tenido que borrar un titulo");
								System.out.println("Dar de alta videojuego");
								juegos.altaTitulos("Dead by dayligth","Behaviour Interactive",2072,"horror","no",19.99,("2016-12-04"));
								juegos.listarTitulos();
								break;
							case 2:
								/*
								 * da de alta una nueva empresa
								 */
								System.out.println("Previamente has tenido que borrar una empresa");
								System.out.println("Dar de alta empresa");
								empresas.altaEmpresa("Behaviour Interactive","Remi Racine",3520,("1992-05-04"));
								empresas.listarEmpresas();
								break;
							case 3:
								/*
								 * da de alta una nueva cuenta
								 */
								System.out.println("Previamente has tenido que borrar una cuenta");
								System.out.println("Dar de alta cuenta");
								usuarios.altaCuenta("ochando","Arturo",26,29.4,("2008-03-07"));
								usuarios.listarCuentas();
								break;
							case 4:
								break;
								default:
							System.out.println("opcion incorrecta");
							break;
							}
						}while(opcion!=4);
					
					break;
				case 5:
					do {
						System.out.println("1-Modificar Titulos");
						System.out.println("2-Modificar empresas");
						System.out.println("3-Modificar Cuentas");
						System.out.println("4-Salir");
						System.out.println("Introduzca el numero de opcion:");
						opcion=num.nextInt();
						
							switch (opcion) {
							case 1:
								/*
								 * llama al metodo cambiar videojuego y luego cambiar empresa para cambiar la empresa del videojuego
								 */
								num.nextLine();
								System.out.println("Introduce el videojuego en el que quieras cambiar la empresa");
								String cambiarVideojuego=num.nextLine();
								System.out.println("Introduce la nueva empresa del videojuego");
								String cambiarEmpresa=num.nextLine();
								juegos.cambiarTitulos(cambiarVideojuego,cambiarEmpresa);
								juegos.listarTitulos();
								break;
							case 2:
								/*
								 * llama al metodo cambiar empresa asociado y luego cambiar asociado para cambiar el asociado de la empresa
								 */
								num.nextLine();
								System.out.println("Introduce la empresa en la que quieras cambiar el asociado");
								String cambiarEmpresaAsociado=num.nextLine();
								System.out.println("Introduce el nuevo asociado de la empresa");
								String cambiarAsociado=num.nextLine();
								empresas.cambiarEmpresas(cambiarEmpresaAsociado,cambiarAsociado);
								empresas.listarEmpresas();
								break;
							case 3:
								/*
								 * llama al metodo cambiar cuenta y luego cambiar nombre para cambiar el nombre de la cuenta
								 */
								num.nextLine();
								System.out.println("Introduce la cuenta en la que quieras cambiar el nombre");
								String cambiarCuenta=num.nextLine();
								System.out.println("Introduce el nuevo nombre de la cuenta");
								String cambiarNombre=num.nextLine();
								usuarios.cambiarCuenta(cambiarCuenta,cambiarNombre);
								usuarios.listarCuentas();
								break;
							case 4:
								break;
								default:
							System.out.println("opcion incorrecta");
							break;
							}
						}while(opcion!=4);
					
					break;
				case 6:
					do {
						System.out.println("1-Listar titulos por empresa");
						System.out.println("2-Listar empresas por asociado");
						System.out.println("3-Listar cuentas por nombre");
						System.out.println("4-Salir");
						System.out.println("Introduzca el numero de opcion:");
						opcion=num.nextInt();
						
							switch (opcion) {
							case 1:
								/*
								 * llama al metodo listar titulo por empresa
								 */
								num.nextLine();
								System.out.println("Listar titulos por empresa");
								String listarTituloPorEmpresa=num.nextLine();
								juegos.listarTitulosPorEmpresa(listarTituloPorEmpresa);
								break;
							case 2:
								/*
								 * llama al metodo listar empresa por asociado
								 */
								num.nextLine();
								System.out.println("Listar empresa por asociado");
								String listarEmpresaPorAsociado=num.nextLine();
								empresas.listarEmpresasPorAsociado(listarEmpresaPorAsociado);
								break;
							case 3:
								/*
								 * llama al metodo listar cuenta por nombre
								 */
								num.nextLine();
								System.out.println("Listar cuenta por nombre");
								String listarCuentaPorNombre=num.nextLine();
								usuarios.listarCuentaPorNombre(listarCuentaPorNombre);
								break;
							case 4:
								break;
								default:
							System.out.println("opcion incorrecta");
							break;
							}
						}while(opcion!=4);
					break;
				case 7:
				System.out.println("El programa ha terminado");
				break;
				default:
				System.out.println("opcion incorrecta");
				break;
				}
			}while(opcion!=7);
			
		
		


		num.close();
	}

}

