package clases3;

import java.time.LocalDate;


public class ListaUsuarios {
private Cuenta[] usuarios;
/*
 * constructor para el maximo de cuentas que va ha tener el programa
 */
	public ListaUsuarios(int maxUsuarios) {
		this.usuarios=new Cuenta[maxUsuarios];
	}
	/*
	 * constructor con todos los atributos de la clase Cuenta
	 */
	public void altaCuenta(String nick,String nombre,int edad,double dineroEnCuenta,String fechaAlta) {
		for(int i=0;i<usuarios.length;i++) {
			if(usuarios[i]==null) {
				usuarios[i]=new Cuenta(nick);
				usuarios[i].setNombre(nombre);
				usuarios[i].setEdad(edad);
				usuarios[i].setDineroEnCuenta(dineroEnCuenta);
				usuarios[i].setFechaAlta(LocalDate.parse(fechaAlta));
				break;
			}
		}
	}
	/*
	 * metodo para buscar las cuentas que se le indique 
	 */
	public Cuenta buscarCuenta(String nick) {
		for(int i=0;i<usuarios.length;i++) {
			if(usuarios[i]!=null) {
			if(usuarios[i].getNick().equals(nick)) {
				return usuarios[i];
			}
		  }
		}
		return null;
	}
	/*
	 * metodo que elimine las cuentas que se le indique
	 */
	public void eliminarCuenta(String nick){
		for(int i=0;i<usuarios.length;i++) {
			if(usuarios[i]!=null) {
			if(usuarios[i].getNick().equals(nick)) {
			usuarios[i]=null;
			}
			}
		}
	}
	/*
	 * metodo que devuelva todas las cuentas
	 */
	public void listarCuentas() {
		for(int i=0;i<usuarios.length;i++) {
			if(usuarios[i]!=null) {
				System.out.println(usuarios[i]);
			}
		}
	}
	/*
	 * metodo que cambie el nombre de la cuenta indicada
	 */
	public void cambiarCuenta(String nick,String nombre) {
		for(int i=0;i<usuarios.length;i++) {
			if(usuarios[i]!=null) {
			if(usuarios[i].getNick().equals(nick)) {
				usuarios[i].setNombre(nombre);
			}
		  }
		}
	}
	/*
	 * metodo que devuelva una lista de cuentas por el nombre que le indique
	 */
	public void listarCuentaPorNombre(String nombre) {
		for(int i=0;i<usuarios.length;i++) {
			if(usuarios[i]!=null) {
			if(usuarios[i].getNombre().equals(nombre)) {
				System.out.println(usuarios[i]);
			}
		  }
		}
	}
	/*
	 * metodo que elimine las cuentas por el nombre que le indique
	 */
	public void eliminarCuentaPorNombre(String nombre) {
		for(int i=0;i<usuarios.length;i++) {
			if(usuarios[i]!=null) {
			if(usuarios[i].getNombre().equals(nombre)) {
				usuarios[i]=null;
			}
		  }
		}
	}
}
