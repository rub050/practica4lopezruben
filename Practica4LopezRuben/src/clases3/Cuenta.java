package clases3;

import java.time.LocalDate;

public class Cuenta {
	
	private String nick;
	private String nombre;
	private int edad;
	private double dineroEnCuenta;
	private LocalDate fechaAlta;
	public Cuenta(String nick) {
		this.nick = nick;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
	public double getDineroEnCuenta() {
		return dineroEnCuenta;
	}
	public void setDineroEnCuenta(double dineroEnCuenta) {
		this.dineroEnCuenta = dineroEnCuenta;
	}
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	@Override
	public String toString() {
		return "La cuenta con nick '" + nick + "' es de " + nombre + " su edad es " + edad + " tiene un saldo en cuenta de " + dineroEnCuenta
				+ " y se creo la cuenta el " + fechaAlta ;
	}
	
	
	
	
}

